/* Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. */

let numeros = [1, 4, 2, 6, 7, 0, 4, 6, 2, 1];
let mayor = "";
let pos_mayor = "";

for (let i = 0; i < numeros.length; i++)
{
    if (i == 0) {
        mayor = numeros[0];
        pos_mayor = 0;
    }
    else if (numeros[i] > mayor)
    {
        mayor = numeros[i];
        pos_mayor = i;
    }
}

console.log(`El nùmero mayor es el ${mayor} y esta en la posiciòn ${pos_mayor} del vector`);