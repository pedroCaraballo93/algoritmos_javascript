/*LLeer un número entero y determinar cuantas veces tiene el digito 1 */

let numero = 1214;
let contador = 0;

numero = numero.toString();

for (let i = 0; i < numero.length; i++) {

    if(numero[i] === '1')
    {
        contador += 1
    }
}

console.log(`En el numero ${numero} el dìgito 1 se repite: ${contador} veces`);