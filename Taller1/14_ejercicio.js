/* Leer un número entero y mostrar en pantalla su tabla de multiplicar */

let numero = 10;

console.log(`------Tabla del ${numero}-------`);

for (let j = 1; j <= 10; j++) 
{
    console.log(`${numero} x ${j} = ${numero * j}`)
}
