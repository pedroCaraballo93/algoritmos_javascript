/* Leer dos numeres enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números*/

let numero1 = 6;
let numero2 = 3;

let diferencia = numero1 - numero2;

if(numero1 % diferencia == 0 && numero2 % diferencia == 0)
{
    console.log(`La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto de ambos numeros`);
}
else if(numero1 % diferencia == 0)
{
    console.log(`La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto del nùmero ${numero1}`);
}
else if(numero2 % diferencia == 0)
{
    console.log(`La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto del nùmero ${numero2}`);
}
else
{
    console.log(`La diferencia de los nùmeros ${numero1} y ${numero2} no es un divisor exacto de ninguno de los numeros`);
}
