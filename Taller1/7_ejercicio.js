/* Leer un número entero de dos digitos y determinar si los dos digitos son iguales.*/

let numero = 24;

if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
{
    const digito1 = parseInt(numero / 10);
    const digito2 = parseInt(numero % 10);

    if(digito1 == digito2)
    {
        console.log(`Los dìgitos que conforman el nùmero ${numero} son iguales`);
    }
    else
    {
        console.log(`Los dìgitos que conforman el nùmero ${numero} no son iguales`);
    }
}
else
{
    console.log("El nùmero debe contener dos digitos");
}