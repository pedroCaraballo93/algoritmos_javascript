/*Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digito */

let numero = 94;

if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
{
    const parteEntera = parseInt(numero / 10);
    const residuo = parseInt(numero % 10);

    const suma = parteEntera + residuo;
    console.log(`La suma de los dìgitos que conforman el nùmero ${numero} es: ${suma}`);
}
else
{
    console.log("El nùmero debe contener dos digitos");
}