/* Leer un número entero de dos digitos y determinar si ambos digitos son pares. */

let numero = 24;

if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
{
    const parteEntera = parseInt(numero / 10);
    const residuo = parseInt(numero % 10);

    if(parteEntera % 2 == 0 && residuo % 2 == 0)
    {
        console.log(`Los dìgitos que conforman el nùmero ${numero} son pares`)
    }
    else
    {
        console.log(`Los dìgitos que conforman el nùmero ${numero} no son ambos pares`)
    }
}
else
{
    console.log("El nùmero debe contener dos digitos");
}