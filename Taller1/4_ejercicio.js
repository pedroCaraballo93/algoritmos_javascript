/* Leer un número entena de dos digitos menor gue 20 y determinar si es primo*/

let numero = 17;

if(numero.toString().length == 2 && numero < 20)
{
    let contador = 0;

    for (let index = 1; index <= numero; index++)
    {
        if(numero % index == 0)
        {
            contador += 1;
        }
    }

    if(contador == 2)
    {
        console.log(`El nùmero ${numero} es primo`);
    }
    else
    {
        console.log(`El nùmero ${numero} no es primo`);
    }
}
else
{
    console.log("El nùmero debe contener dos digitos y ser menor que 20");
}