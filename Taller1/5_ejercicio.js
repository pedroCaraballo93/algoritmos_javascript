/* Leer un número entero de dos digitos y determinar si es primo y además si es negativo */

let numero = -13;

if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
{
    let contador = 0;

    for (let index = 1; index <= numero; index++)
    {
        if(numero % index == 0)
        {
            contador += 1;
        }
    }

    if(contador == 2 && numero < 0)
    {
        console.log(`El nùmero ${numero} es primo y negativo`);
    }
    else if(contador == 2 && numero > 0)
    {
        console.log(`El nùmero ${numero} es primo y positivo`);
    }
    else
    {
        console.log(`El nùmero ${numero} no es primo`);
    }
}
else
{
    console.log("El nùmero debe contener dos digitos");
}