/* Leer un número entero y determinar si es negativo. */

let numero = 300;

if(numero < 0)
{
    console.log("El nùmero es negativo");
}
else if(numero > 0)
{
    console.log("El nùmero es positivo");
}
else
{
    console.log("El cero es considerado positivo y negativo");
}