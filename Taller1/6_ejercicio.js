/* Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro*/

let numero = 24;

if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
{
    const digito1 = parseInt(numero / 10);
    const digito2 = parseInt(numero % 10);

    if(digito1 % digito2 == 0)
    {
        console.log(`El digito ${digito1} es multiplo del digito ${digito2}`);
    }
    else if(digito2 % digito1 == 0)
    {
        console.log(`El digito ${digito2} es multiplo del digito ${digito1}`);
    }
    else
    {
        console.log(`Ninguno de los digitos del numero ${numero} es multiplo del otro`);
    }
}
else
{
    console.log("El nùmero debe contener dos digitos");
}