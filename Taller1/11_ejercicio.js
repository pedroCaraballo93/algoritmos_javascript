/*Leer un número entero y determinar a cuánto es igual la suma de sus digitos */

let numero = 1234;
let suma = 0;

numero = numero.toString();

for (let i = 0; i < numero.length; i++) {

    suma += parseInt(numero[i])
}

console.log(`La suma de los digitos del numero ${numero} es: ${suma}`);