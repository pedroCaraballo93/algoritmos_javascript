const ordenar_contar_pokemones = require('./t2_ejercicio2.js');

const pokemones = [
  "Bulbasaur",
  "Pikachu",
  "Vulpix",
  "Ninetales",
  "Parasect",
  "Venonat"
];


test('Ordenar lista de pokemones', () => {
  expect(ordenar_contar_pokemones(pokemones)).toContain("Bulbasaur", "Ninetales", "Parasect", "Pikachu", "Venonat", "Vulpix");
});




