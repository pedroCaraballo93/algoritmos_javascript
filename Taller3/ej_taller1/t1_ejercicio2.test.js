const sumar_digitos = require('./t1_ejercicio2.js');

test('La suma de los digitos de 35 es 8', () => {
  expect(sumar_digitos(35)).toBe(8);
});

test('La suma de los digitos de -94 es -13', () => {
    expect(sumar_digitos(-94)).toBe(-13);
  });

test('La suma de los digitos de 350 no es valida', () => {
    expect(sumar_digitos(350)).toBe("El nùmero debe contener dos digitos");
  });


