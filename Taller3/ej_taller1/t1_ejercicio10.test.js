const diferencia_divisor = require('./t1_ejercicio10.js');

test('La diferencia de 4 y 2 es divisor exacto de ambos numeros', () => {
  expect(diferencia_divisor(4, 2)).toBe("La diferencia de los nùmeros 4 y 2 es un divisor exacto de ambos numeros");
});

test('La diferencia de 3 y 1 no es divisor exacto de ambos digitos', () => {
  expect(diferencia_divisor(3, 1)).toBe("La diferencia de los nùmeros 3 y 1 no es un divisor exacto de ninguno de los numeros");
});

test('La diferencia de 20 y 20 no es divisor exacto de ambos digitos', () => {
  expect(diferencia_divisor(20, 20)).toBe("La diferencia de los nùmeros 20 y 20 no es un divisor exacto de ninguno de los numeros");
});







