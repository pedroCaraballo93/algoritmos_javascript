const digitos_iguales = require('./t1_ejercicio8.js');

test('El numero 4 no tiene dos digitos', () => {
  expect(digitos_iguales(40000)).toBe("El nùmero debe contener cuatro digitos");
});

test('Los digitos del numero 2330 son iguales', () => {
    expect(digitos_iguales(2330)).toBe("El segundo y el penùltimo dìgito del nùmero 2330 son iguales");
  });

test('Los digitos del numero -5101 no son iguales', () => {
    expect(digitos_iguales(-5101)).toBe("El segundo y el penùltimo dìgito del nùmero -5101 no son iguales");
});

test('Los digitos del numero -2330 son iguales', () => {
    expect(digitos_iguales(-2330)).toBe("El segundo y el penùltimo dìgito del nùmero -2330 son iguales");
});




