/*Leer un número entero y determinar cuantas veces tiene el digito 1 */

function contar_unos(numero) {

    let contador = 0;

    numero = numero.toString();

    for (let i = 0; i < numero.length; i++) {

        if(numero[i] === '1')
        {
            contador += 1
        }
    }

    return contador;

}

module.exports = contar_unos;

