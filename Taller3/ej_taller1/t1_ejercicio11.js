/*Leer un número entero y determinar a cuánto es igual la suma de sus digitos */

function sumar_digitos(numero) {

    let suma = 0;
    let negativo = false;

    if(numero < 0)
    {
        numero = numero * -1;
        negativo = true;
    }

    numero = numero.toString();

    for (let i = 0; i < numero.length; i++) {

        suma += parseInt(numero[i])

        if(i == 0 && negativo)
        {
            suma = suma * -1;
        }
    }

    return suma;

}

module.exports = sumar_digitos;