/*Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digito */

function sumar_digitos(numero) {

    if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
    {
        const parteEntera = parseInt(numero / 10);
        const residuo = parseInt(numero % 10);

        const suma = parteEntera + residuo;
        return suma;
    }
    else
    {
        return "El nùmero debe contener dos digitos";
    }

}

module.exports = sumar_digitos;

