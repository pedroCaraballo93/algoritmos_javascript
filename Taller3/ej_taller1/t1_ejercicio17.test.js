const contador_mayor = require('./t1_ejercicio17.js');

test('El mayor es el 100 y se repite una vez', () => {
  expect(contador_mayor([1, 4, 20, 6, 7, 0, 4, 60, 2, 100])).toBe(1);
});

test('El mayor es el 100 y se repite 3 veces', () => {
  expect(contador_mayor([1, 100, 20, 6, 7, 100, 40, 60, 2, 100])).toBe(3);
});




