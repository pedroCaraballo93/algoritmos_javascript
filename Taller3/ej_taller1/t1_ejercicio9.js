/* Leer tres numeros enteros y determinar si el último digito de los tres numeros es igual.*/

function ultimo_digito_igual(numero1, numero2, numero3)
{
    let mensaje = "El ùltimo dìgito de los tres nùmeros no es igual";
    let numero_aux = numero1;
    let numero_aux2 = numero2;
    let numero_aux3 = numero3;

    if (numero1 < 0) {
        numero_aux *= -1;
    }
    else if (numero2 < 0){
        numero_aux2 *= -1;
    }
    else if (numero3 < 0){
        numero_aux3 *= -1;
    }

    numero_aux = numero_aux.toString();
    numero_aux2 = numero_aux2.toString();
    numero_aux3 = numero_aux2.toString();

    const digito1 = numero_aux.substring(numero_aux.length - 1);
    const digito2 = numero_aux2.substring(numero_aux2.length - 1);
    const digito3 = numero_aux3.substring(numero_aux3.length - 1);

    if(digito1 == digito2 && digito2 == digito3)
    {
        mensaje = `El ùltimo dìgito de los tres nùmeros es igual`;
    }

    return mensaje;

}

module.exports = ultimo_digito_igual;


