const number_is_negative = require('./t1_ejercicio1.js');

test('-350 es negativo', () => {
  expect(number_is_negative(-350)).toBe("El nùmero es negativo");
});

test('350 es positivo', () => {
    expect(number_is_negative(350)).toBe("El nùmero es positivo");
  });

test('0 es positivo y negativo', () => {
    expect(number_is_negative(0)).toBe("El cero es considerado positivo y negativo");
});