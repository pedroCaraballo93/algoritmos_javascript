/*Leer 10 números enteros, almacenados en un vector y determinar cuántas veces está recetido el mayor*/

let numeros = [1, 4, 2, 6, 7, 0, 4, 7, 2, 7];

function contador_mayor(numeros){

    let mayor = "";
    let contador = 0;

    // obtengo el mayor del vector
    for (let i = 0; i < numeros.length; i++)
    {
        if (i == 0) {
            mayor = numeros[0];
        }
        else if (numeros[i] > mayor)
        {
            mayor = numeros[i];
        }
    }

    // cuento cuantas veces se repite el mayor
    for (let i = 0; i < numeros.length; i++)
    {
        if (numeros[i] == mayor) {
            contador += 1;
        }
    }

    return contador;

}

module.exports = contador_mayor;
