/* Leer un número entero de dos digitos menor gue 20 y determinar si es primo*/

function numero_primo(numero)
{
    let mensaje = "El nùmero debe contener dos digitos y ser menor que 20";

    if(numero >= 10 && numero < 20)
    {
        let contador = 0;

        for (let index = 1; index <= numero; index++)
        {
            if(numero % index == 0)
            {
                contador += 1;
            }
        }

        if(contador == 2)
        {
            mensaje = `El nùmero ${numero} es primo`;
        }
        else
        {
            mensaje = `El nùmero ${numero} no es primo`;
        }
    }

    return mensaje;
}

module.exports = numero_primo;