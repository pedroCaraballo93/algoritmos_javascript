/* Leer dos numeres enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números*/

function diferencia_divisor(numero1, numero2)
{
    let diferencia = numero1 - numero2;
    let mensaje = `La diferencia de los nùmeros ${numero1} y ${numero2} no es un divisor exacto de ninguno de los numeros`;


    if(diferencia != 0)
    {
        if(numero1 % diferencia == 0 && numero2 % diferencia == 0)
        {
            mensaje = `La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto de ambos numeros`;
        }
        else if(numero1 % diferencia == 0)
        {
            mensaje = `La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto del nùmero ${numero1}`;
        }
        else if(numero2 % diferencia == 0)
        {
            mensaje = `La diferencia de los nùmeros ${numero1} y ${numero2} es un divisor exacto del nùmero ${numero2}`;
        }
    }

    return mensaje;

}

module.exports = diferencia_divisor;


