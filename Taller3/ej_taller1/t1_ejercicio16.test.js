const posiciones_ceros = require('./t1_ejercicio16.js');

test('Posicion con ceros', () => {
  expect(posiciones_ceros([1, 4, 20, 6, 7, 0, 4, 60, 2, 100])).toContain(2, 5, 7, 9);
});

test('Posiciones con ceros', () => {
  expect(posiciones_ceros([1, 40, 20, 6, 7, 0, 40, 60, 2, 100])).toContain(1, 2, 5, 6, 7, 9);
});




