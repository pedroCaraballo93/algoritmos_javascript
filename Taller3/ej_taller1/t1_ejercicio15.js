/* Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. */

function posicion_mayor(numeros){

    let mayor = "";
    let pos_mayor = "";

    for (let i = 0; i < numeros.length; i++)
    {
        if (i == 0) {
            mayor = numeros[0];
            pos_mayor = 0;
        }
        else if (numeros[i] > mayor)
        {
            mayor = numeros[i];
            pos_mayor = i;
        }
    }

    return pos_mayor;

}

module.exports = posicion_mayor;


