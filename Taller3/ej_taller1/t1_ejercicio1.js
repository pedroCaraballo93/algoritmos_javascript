/* Leer un número entero y determinar si es negativo. */

function number_is_negative(numero){

    let mensaje = "El cero es considerado positivo y negativo";

    if(numero < 0)
    {
        mensaje = "El nùmero es negativo";
    }
    else if(numero > 0)
    {
        mensaje = "El nùmero es positivo";
    }

    return mensaje;

}

module.exports = number_is_negative;
