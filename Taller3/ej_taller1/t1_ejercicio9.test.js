const ultimo_digito_igual = require('./t1_ejercicio9.js');

test('El numero 4 no tiene dos digitos', () => {
  expect(ultimo_digito_igual(40000, 50, 0)).toBe("El ùltimo dìgito de los tres nùmeros es igual");
});

test('Los digitos del numero 2330 son iguales', () => {
    expect(ultimo_digito_igual(2330, 4, 450)).toBe("El ùltimo dìgito de los tres nùmeros no es igual");
  });

test('Los digitos del numero -5101 no son iguales', () => {
    expect(ultimo_digito_igual(-235, 5, 55)).toBe("El ùltimo dìgito de los tres nùmeros es igual");
});






