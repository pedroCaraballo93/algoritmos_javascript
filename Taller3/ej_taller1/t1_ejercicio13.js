/* Generar todas las tablas de multiplicar del 1 al 10 */

function tablas_multiplicar(){

    let mensaje = "";
    let tabla = [];

    try {

        for (let i = 1; i <= 10; i++) {

            // console.log(`------Tabla del ${i}-------`);
            for (let j = 1; j <= 10; j++) {
        
                // console.log(`${i} x ${j} = ${i*j}`);
                tabla.push(`${i} x ${j} = ${i*j}`);
            }
        }

        mensaje = "Las tablas fueron generadas";
        
    } catch (error) {
        mensaje = "Ocurrio un error";
        
    }

    return mensaje;

}

module.exports = tablas_multiplicar;
