const numero_primo = require('./t1_ejercicio4.js');

test('El numero 17 es primo', () => {
  expect(numero_primo(17)).toBe("El nùmero 17 es primo");
});

test('El nùmero 15 no es primo', () => {
    expect(numero_primo(15)).toBe("El nùmero 15 no es primo");
  });

test('El numero 5 no es un numero de dos digitos menor que 20', () => {
    expect(numero_primo(5)).toBe("El nùmero debe contener dos digitos y ser menor que 20");
  });




