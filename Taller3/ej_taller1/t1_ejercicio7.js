/* Leer un número entero de dos digitos y determinar si los dos digitos son iguales.*/

function digitos_iguales(numero) {

    let mensaje = "El nùmero debe contener dos digitos";

    if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
    {
        const digito1 = parseInt(numero / 10);
        const digito2 = parseInt(numero % 10);

        if(digito1 == digito2)
        {
            mensaje = `Los dìgitos que conforman el nùmero ${numero} son iguales`;
        }
        else
        {
            mensaje = `Los dìgitos que conforman el nùmero ${numero} no son iguales`;
        }
    }

    return mensaje;

}

module.exports = digitos_iguales;

