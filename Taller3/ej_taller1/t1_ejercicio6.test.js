const numero_multiplo = require('./t1_ejercicio6.js');

test('El digito 4 es multiplo del digito 2', () => {
  expect(numero_multiplo(42)).toBe("El digito 4 es multiplo del digito 2");
});

test('Los digitos del numero 35 no son multiplos el uno del otro', () => {
    expect(numero_multiplo(35)).toBe("Ninguno de los digitos del numero 35 es multiplo del otro");
  });

test('El numero 500 no es un numero de dos digitos', () => {
    expect(numero_multiplo(500)).toBe("El nùmero debe contener dos digitos");
  });




