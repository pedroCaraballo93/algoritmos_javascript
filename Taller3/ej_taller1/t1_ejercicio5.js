/* Leer un número entero de dos digitos y determinar si es primo y además si es negativo */

function numero_primo_negativo(numero) {

    let mensaje = "El nùmero debe contener dos digitos";

    if(numero.toString().length == 2 || (numero.toString().length == 3 && numero.toString()[0] == "-"))
    {
        let contador = 0;

        // cuento los divisores que tiene el numero
        for (let index = 1; index <= numero; index++)
        {
            if(numero % index == 0)
            {
                contador += 1;
            }
        }

        if(numero < 0)
        {
            mensaje = `El nùmero ${numero} es negativo por lo tanto no puede ser primo`;
        }
        else if(contador == 2 && numero > 0)
        {
            mensaje = `El nùmero ${numero} es primo y positivo`;
        }
        else
        {
            mensaje = `El nùmero ${numero} no es primo`;
        }
    }

    return mensaje;

}

module.exports = numero_primo_negativo;
