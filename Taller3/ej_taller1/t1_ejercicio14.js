/* Leer un número entero y mostrar en pantalla su tabla de multiplicar */

function tabla_numero(numero)
{
    let tabla = [];

    for (let j = 1; j <= 10; j++) 
    {
        tabla.push(numero * j);
    }

    return tabla;

}

module.exports = tabla_numero;


