const contar_unos = require('./t1_ejercicio12.js');

test('El 123 tiene un uno', () => {
  expect(contar_unos(123)).toBe(1);
});

test('El numero -111 tiene 3 unos', () => {
  expect(contar_unos(-111)).toBe(3);
});


test('El 987 no tiene unos', () => {
  expect(contar_unos(987)).toBe(0);
});





