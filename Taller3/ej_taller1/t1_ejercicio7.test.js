const digitos_iguales = require('./t1_ejercicio7.js');

test('El numero 4 no tiene dos digitos', () => {
  expect(digitos_iguales(4)).toBe("El nùmero debe contener dos digitos");
});

test('Los digitos del numero 33 son iguales', () => {
    expect(digitos_iguales(33)).toBe("Los dìgitos que conforman el nùmero 33 son iguales");
  });

test('Los digitos del numero -50 no son iguales', () => {
    expect(digitos_iguales(-50)).toBe("Los dìgitos que conforman el nùmero -50 no son iguales");
  });




