const posicion_mayor = require('./t1_ejercicio15.js');

test('Posicion del mayor = 4', () => {
  expect(posicion_mayor([1, 4, 2, 6, 7, 0, 4, 6, 2, 1])).toBe(4);
});

test('Posicion del mayor = 8', () => {
  expect(posicion_mayor([1, 4, 2, 6, 0, 0, 4, 6, 10, 10])).toBe(8);
});




