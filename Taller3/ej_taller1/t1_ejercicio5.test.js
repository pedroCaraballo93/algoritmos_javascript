const numero_primo_negativo = require('./t1_ejercicio5.js');

test('El numero 17 es primo y positivo', () => {
  expect(numero_primo_negativo(17)).toBe("El nùmero 17 es primo y positivo");
});

test('El nùmero 15 no es primo', () => {
    expect(numero_primo_negativo(15)).toBe("El nùmero 15 no es primo");
  });

test('El numero 5 no es un numero de dos digitos', () => {
    expect(numero_primo_negativo(5)).toBe("El nùmero debe contener dos digitos");
  });

test('El nùmero -15 no es primo porque es negativo', () => {
  expect(numero_primo_negativo(-15)).toBe("El nùmero -15 es negativo por lo tanto no puede ser primo");
});




