/* Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentran los números terminados en O*/

let numeros = [1, 4, 20, 6, 7, 0, 4, 60, 2, 100];

function posiciones_ceros(numeros) {

    let posiciones_ceros = [];

    for (let i = 0; i < numeros.length; i++)
    {
        if (numeros[i] == 0 || numeros[i] % 10 == 0) 
        {
            posiciones_ceros.push(i);
        }
    }

    return posiciones_ceros;

}

module.exports = posiciones_ceros;




