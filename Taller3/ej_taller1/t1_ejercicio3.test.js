const verificar_pares = require('./t1_ejercicio3.js');

test('Los digitos de 35 no son pares', () => {
  expect(verificar_pares(35)).toBe("Los dìgitos que conforman el nùmero 35 no son ambos pares");
});

test('Los digitos de -64 son pares', () => {
    expect(verificar_pares(-64)).toBe("Los dìgitos que conforman el nùmero -64 son pares");
  });

test('Los digitos de 28 son pares', () => {
    expect(verificar_pares(28)).toBe("Los dìgitos que conforman el nùmero 28 son pares");
});



