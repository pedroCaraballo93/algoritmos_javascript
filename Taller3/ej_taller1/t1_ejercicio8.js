/* Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo*/

function digitos_iguales(numero) {

    let mensaje = "El nùmero debe contener cuatro digitos";

    if(numero.toString().length == 4 || (numero.toString().length == 5 && numero.toString()[0] == "-"))
    {
        let numero_aux = numero;

        if (numero < 0) {
            numero_aux *= -1;
        }

        const digito1 = numero_aux.toString().charAt(1);
        const digito2 = numero_aux.toString().charAt(2);

        if(digito1 == digito2)
        {
            mensaje = `El segundo y el penùltimo dìgito del nùmero ${numero} son iguales`;
        }
        else
        {
            mensaje = `El segundo y el penùltimo dìgito del nùmero ${numero} no son iguales`;
        }
    }
   
    return mensaje;

}

module.exports = digitos_iguales;

