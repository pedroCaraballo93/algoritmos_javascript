const sumar_digitos = require('./t1_ejercicio11.js');

test('La suma de los digitos de 123 es 6', () => {
  expect(sumar_digitos(123)).toBe(6);
});

test('La suma de los digitos de -123 es 6', () => {
  expect(sumar_digitos(-123)).toBe(4);
});





